package br.com.rabobank.statementprocessor.service.impl;

import br.com.rabobank.statementprocessor.dto.Statement;
import br.com.rabobank.statementprocessor.dto.ValidationResponse;
import br.com.rabobank.statementprocessor.exception.GenericException;
import br.com.rabobank.statementprocessor.exception.PropertyValidationException;
import br.com.rabobank.statementprocessor.service.FileProcessingService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;


@Service
@RequiredArgsConstructor
public class CSVService implements FileProcessingService {

    private static final Logger log = LoggerFactory.getLogger(CSVService.class);

    private final FileValidationService fileValidationService;

    @Override
    public List<ValidationResponse> process(MultipartFile file) throws PropertyValidationException {
        log.info(String.format("process: %s", file.getOriginalFilename()));
        Pattern pattern = Pattern.compile(",");
        int lineCounter = 0;
        try (BufferedReader in = new BufferedReader(new InputStreamReader(file.getInputStream()))) {
            List<Statement> statementList = new ArrayList<>();
            String line;
            while ((line = in.readLine()) != null) {
                if (lineCounter > 0) {
                    String[] parametersList = pattern.split(line);
                    statementList.add(fileValidationService.validateInput(parametersList));
                }
                lineCounter++;
            }
            return fileValidationService.validateStatement(statementList, file);
        } catch (PropertyValidationException ex) {
            throw ex;
        }catch (IOException e) {
            throw new GenericException();
        }
    }

    @Override
    public String getName() {
        return "CSV";
    }

}
