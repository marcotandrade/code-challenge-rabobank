package br.com.rabobank.statementprocessor.service;

import br.com.rabobank.statementprocessor.dto.Statement;
import br.com.rabobank.statementprocessor.dto.ValidationResponse;
import br.com.rabobank.statementprocessor.exception.PropertyValidationException;
import br.com.rabobank.statementprocessor.model.StatementEntity;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface FileValidation {

 List<ValidationResponse> validateStatement(List<Statement>  statement, MultipartFile file);

 Statement validateInput(String[] line) throws PropertyValidationException;

 List<Statement> validateInput(List<Statement> statementList) throws PropertyValidationException;


}
