package br.com.rabobank.statementprocessor.service.impl;

import br.com.rabobank.statementprocessor.dto.Statement;
import br.com.rabobank.statementprocessor.dto.Records;
import br.com.rabobank.statementprocessor.dto.ValidationResponse;
import br.com.rabobank.statementprocessor.exception.GenericException;
import br.com.rabobank.statementprocessor.exception.PropertyValidationException;
import br.com.rabobank.statementprocessor.service.FileProcessingService;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;


@Service
@RequiredArgsConstructor
public class XMLService implements FileProcessingService {

    private static final Logger log = LoggerFactory.getLogger(XMLService.class);

    private final FileValidationService fileValidationService;

    @Override
    public List<ValidationResponse> process(MultipartFile file) throws PropertyValidationException {
        log.info(String.format("process: %s", file.getOriginalFilename()));
        try {
            String xmlContent = new String(file.getBytes());
            XmlMapper xmlMapper = new XmlMapper();
            Records records = xmlMapper.readValue(xmlContent, Records.class);
            List<Statement> statementList = new ArrayList<>();
            records.getRecord().forEach(rec -> statementList.add(Statement.recordToStatement(rec)));
            List<Statement> validStatementList = fileValidationService.validateInput(statementList);
            return fileValidationService.validateStatement(validStatementList, file);
        } catch (PropertyValidationException ex){
            throw ex;
        } catch (Exception ex){
            log.error(ex.getMessage());
            throw new GenericException();
        }
    }

    @Override
    public String getName() {
        return "XML";
    }
}
