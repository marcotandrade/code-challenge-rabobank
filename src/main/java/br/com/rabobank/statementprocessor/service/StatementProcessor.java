package br.com.rabobank.statementprocessor.service;

import br.com.rabobank.statementprocessor.dto.ValidationResponse;
import br.com.rabobank.statementprocessor.exception.PropertyValidationException;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface StatementProcessor {

    List<ValidationResponse> process(MultipartFile file) throws PropertyValidationException;

}
