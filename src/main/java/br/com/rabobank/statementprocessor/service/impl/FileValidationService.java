package br.com.rabobank.statementprocessor.service.impl;

import br.com.rabobank.statementprocessor.dto.Statement;
import br.com.rabobank.statementprocessor.dto.ValidationResponse;
import br.com.rabobank.statementprocessor.exception.PropertyValidationException;
import br.com.rabobank.statementprocessor.model.FileEntity;
import br.com.rabobank.statementprocessor.model.StatementEntity;
import br.com.rabobank.statementprocessor.repository.FileRepository;
import br.com.rabobank.statementprocessor.repository.StatementRepository;
import br.com.rabobank.statementprocessor.service.FileValidation;
import br.com.rabobank.statementprocessor.utils.InputFilter;
import br.com.rabobank.statementprocessor.utils.RegularExpression;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@Service
@RequiredArgsConstructor
public class FileValidationService implements FileValidation {

    private static final Logger log = LoggerFactory.getLogger(FileValidationService.class);

    private final StatementRepository statementRepository;
    private final FileRepository fileRepository;

    @Override
    public List<ValidationResponse> validateStatement(List<Statement> statements, MultipartFile file) {
        log.info(String.format("validateStatement: %s", statements.toString()));
        List<StatementEntity> statementEntities = new ArrayList<>();
        FileEntity fileEntity = FileEntity.builder().name(file.getOriginalFilename()).build();
        statements.forEach(st -> statementEntities.add(balanceValidationProcess(st, fileEntity)));
        fileEntity.setStatementEntityList(statementEntities);
        FileEntity persistedFile = fileRepository.save(fileEntity);
        HashSet<StatementEntity> records = statementRepository.findInvalidRecords(persistedFile.getId());
        List<ValidationResponse> responseList = new ArrayList<>();
        records.forEach(rec -> responseList.add(ValidationResponse.toValidationResponse(rec)));
        return responseList;
    }




    @Override
    public Statement  validateInput(String[] line) throws PropertyValidationException {
        String reference = line[0].trim();
        String accountNumber = line[1].trim();
        String description = line[2].trim();
        String startBalance = line[3].trim();
        String mutation = line[4].trim();
        String endBalance = line[5].trim();

        boolean referenceOK = InputFilter.validateInput(reference, RegularExpression.VALIDATE_ONLY_NUMBER);
        boolean accountNumberOK = InputFilter.validateInput(accountNumber, RegularExpression.VALIDATE_ALPHANUMERIC);
        boolean startBalanceOK = InputFilter.validateInput(startBalance, RegularExpression.VALIDATE_BALANCE);
        boolean mutationOK = InputFilter.validateInput(mutation, RegularExpression.VALIDATE_MUTATION);
        boolean endBalanceOK = InputFilter.validateInput(endBalance, RegularExpression.VALIDATE_BALANCE);

        if(referenceOK && accountNumberOK && startBalanceOK && mutationOK && endBalanceOK){
            return Statement.builder()
                    .reference(Long.valueOf(reference))
                    .description(description)
                    .accountNumber(accountNumber)
                    .startBalance(startBalance)
                    .mutation(mutation)
                    .endBalance(endBalance)
                    .build();
        }else{
            throw new PropertyValidationException();
        }
    }

    @Override
    public List<Statement> validateInput(List<Statement> statementList) throws PropertyValidationException {
        log.info(String.format("validateInput: %s", statementList.toString()));
        for(Statement st : statementList) {
            boolean referenceOK = InputFilter.validateInput(st.getReference().toString(), RegularExpression.VALIDATE_ONLY_NUMBER);
            boolean accountNumberOK = InputFilter.validateInput(st.getAccountNumber(), RegularExpression.VALIDATE_ALPHANUMERIC);
            boolean startBalanceOK = InputFilter.validateInput(st.getStartBalance(), RegularExpression.VALIDATE_BALANCE);
            boolean mutationOK = InputFilter.validateInput(st.getMutation(), RegularExpression.VALIDATE_MUTATION);
            boolean endBalanceOK = InputFilter.validateInput(st.getEndBalance(), RegularExpression.VALIDATE_BALANCE);

            if (!referenceOK || !accountNumberOK || !startBalanceOK || !mutationOK || !endBalanceOK) {
                throw new PropertyValidationException();
            }
        }
        return statementList;
    }


    StatementEntity balanceValidationProcess(Statement statement, FileEntity file){
        StatementEntity statementEntity =
                StatementEntity.builder()
                        .reference(statement.getReference())
                        .accountNumber(statement.getAccountNumber())
                        .description(statement.getDescription())
                        .startBalance(statement.getStartBalance())
                        .mutation(statement.getMutation())
                        .endBalance(statement.getEndBalance())
                        .valid(false)
                        .file(file)
                        .build();

        String balanceValidation = validateEndBalance(statement.getStartBalance(), statement.getMutation(), statement.getEndBalance());
        if(null == balanceValidation){
            statementEntity.setValid(true);
        }else{
            statementEntity.setErrorDescription(balanceValidation);
        }
        return statementEntity;
    }

    private String validateEndBalance(String startBalance, String mutation, String endBalance){
        BigDecimal initialBalance = new BigDecimal(startBalance);
        BigDecimal finalBalance = new BigDecimal(endBalance);
        BigDecimal mutationValue = new BigDecimal(mutation);

        BigDecimal expectedValue = initialBalance.add(mutationValue);
        int response = expectedValue.compareTo(finalBalance);
        if(response == 0){
            return null;
        }else{
            return "Invalid end balance";
        }
    }

}
