package br.com.rabobank.statementprocessor.service.impl;

import br.com.rabobank.statementprocessor.dto.ValidationResponse;
import br.com.rabobank.statementprocessor.exception.FileNotSupportedException;
import br.com.rabobank.statementprocessor.exception.GenericException;
import br.com.rabobank.statementprocessor.exception.PropertyValidationException;
import br.com.rabobank.statementprocessor.service.FileProcessingService;
import br.com.rabobank.statementprocessor.service.StatementProcessor;
import br.com.rabobank.statementprocessor.enums.FileType;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
@RequiredArgsConstructor
public class StatementProcessorService implements StatementProcessor {

    @Override
    public List<ValidationResponse> process(MultipartFile file) throws PropertyValidationException {
        try {
            FileType fileType = FileType.lookup(file.getContentType());
            FileProcessingService fileService = FileServiceFactory.getService(fileType);
            return fileService.process(file);
        } catch (FileNotSupportedException | PropertyValidationException ex){
            throw ex;
        } catch (Exception ex){
            throw new GenericException();
        }
    }
}
