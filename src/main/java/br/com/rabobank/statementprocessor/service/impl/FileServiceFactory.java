package br.com.rabobank.statementprocessor.service.impl;

import br.com.rabobank.statementprocessor.service.FileProcessingService;
import br.com.rabobank.statementprocessor.enums.FileType;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class FileServiceFactory {

    private final List<FileProcessingService> services;

    private static final Map<String, FileProcessingService> fileServiceCache = new HashMap<>();

    @PostConstruct
    public void initServiceCache() {
        for(FileProcessingService service : services) {
            fileServiceCache.put(service.getName(), service);
        }
    }

    public static FileProcessingService getService(FileType fileType) {
        return fileServiceCache.get(fileType.name());
    }

}
