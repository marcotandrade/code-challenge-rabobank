package br.com.rabobank.statementprocessor.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import java.time.LocalDateTime;


@Data
@Builder
@Entity
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "statement")
public class StatementEntity extends DefaultEntity {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "id", length = 20)
    private Long id;

    @Column(name = "reference", length = 20)
    private Long reference;

    @Column(name = "account_number", length = 20)
    private String accountNumber;

    @Column(name = "description")
    private String description;

    @Column(name = "start_balance", length = 20)
    private String startBalance;

    @Column(name = "mutation", length = 10)
    private String mutation;

    @Column(name = "end_balance", length = 20)
    private String endBalance;

    @Column(name = "valid")
    private Boolean valid;

    @Column(name = "error_description")
    private String errorDescription;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "file_id")
    private FileEntity file;


}
