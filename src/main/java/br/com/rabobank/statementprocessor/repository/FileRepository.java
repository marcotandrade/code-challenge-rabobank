package br.com.rabobank.statementprocessor.repository;

import br.com.rabobank.statementprocessor.model.FileEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface FileRepository extends JpaRepository<FileEntity, Long> {


}
