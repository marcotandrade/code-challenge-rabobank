package br.com.rabobank.statementprocessor.repository;

import br.com.rabobank.statementprocessor.model.FileEntity;
import br.com.rabobank.statementprocessor.model.StatementEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.HashSet;
import java.util.List;

@Repository
public interface StatementRepository extends JpaRepository<StatementEntity, Long> {

    @Query(nativeQuery = true,
        value = "SELECT  *\n" +
                "  FROM  STATEMENT\n" +
                " WHERE  VALID = FALSE\n" +
                "   AND  FILE_ID = :fileId\n" +
                " UNION  ALL\n" +
                "SELECT  *\n" +
                "  FROM  STATEMENT\n" +
                " WHERE  REFERENCE IN(SELECT REFERENCE FROM STATEMENT WHERE FILE_ID = :fileId GROUP BY REFERENCE HAVING COUNT(REFERENCE) > 1)    \n" +
                "        AND FILE_ID = :fileId\n" +
                " UNION  ALL\n" +
                "SELECT * FROM  STATEMENT\n" +
                "WHERE REFERENCE IN (\n" +
                " SELECT  REFERENCE\n" +
                "  FROM  STATEMENT\n" +
                " WHERE  REFERENCE IN(SELECT REFERENCE FROM STATEMENT WHERE FILE_ID = :fileId GROUP BY REFERENCE HAVING COUNT(REFERENCE) = 1)    \n" +
                "        AND FILE_ID <> :fileId)\n" +
                "        AND FILE_ID = :fileId")
    HashSet<StatementEntity> findInvalidRecords(@Param("fileId") Long fileId);

}
