package br.com.rabobank.statementprocessor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@EnableAsync
@EnableAutoConfiguration
@SpringBootApplication
public class StatementprocessorApplication {

	public static void main(String[] args) {
		SpringApplication.run(StatementprocessorApplication.class, args);
	}

}
