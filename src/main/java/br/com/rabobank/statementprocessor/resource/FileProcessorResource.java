package br.com.rabobank.statementprocessor.resource;


import br.com.rabobank.statementprocessor.dto.ValidationResponse;
import br.com.rabobank.statementprocessor.exception.PropertyValidationException;
import br.com.rabobank.statementprocessor.service.StatementProcessor;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
@RequestMapping("/file")
@RequiredArgsConstructor
public class FileProcessorResource {

    private final StatementProcessor statementProcessor;

    @PostMapping
    public ResponseEntity<List<ValidationResponse>> fileProcess(@RequestParam("file") MultipartFile file) throws PropertyValidationException {
        List<ValidationResponse> response = statementProcessor.process(file);
        return ResponseEntity.ok().body(response);
    }

}
