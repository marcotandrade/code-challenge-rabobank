package br.com.rabobank.statementprocessor.dto;

import br.com.rabobank.statementprocessor.model.StatementEntity;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ValidationResponse {

    private Long reference;
    private String description;

    public static ValidationResponse toValidationResponse(StatementEntity statement){
        return ValidationResponse.builder()
                .reference(statement.getReference())
                .description(statement.getDescription())
                .build();
    }

}
