package br.com.rabobank.statementprocessor.dto;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class Record implements Serializable {

    @JacksonXmlProperty(localName = "reference", isAttribute = true)
    private Long reference;

    @JacksonXmlProperty(localName = "accountNumber")
    private String accountNumber;

    @JacksonXmlProperty(localName = "description")
    private String description;

    @JacksonXmlProperty(localName = "startBalance")
    private String startBalance;

    @JacksonXmlProperty(localName = "mutation")
    private String mutation;

    @JacksonXmlProperty(localName = "endBalance")
    private String endBalance;

    public Record() {
    }
}
