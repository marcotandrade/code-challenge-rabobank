package br.com.rabobank.statementprocessor.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Statement {

    private Long reference;
    private String accountNumber;
    private String description;
    private String startBalance;
    private String mutation;
    private String endBalance;
    private String errorDescription;

    public static Statement recordToStatement(Record record){
        return Statement.builder()
                .reference(record.getReference())
                .accountNumber(record.getAccountNumber())
                .description(record.getDescription())
                .startBalance(record.getStartBalance())
                .mutation(record.getMutation())
                .endBalance(record.getEndBalance())
                .build();
    }

}
