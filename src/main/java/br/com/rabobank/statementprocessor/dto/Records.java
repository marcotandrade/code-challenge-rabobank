package br.com.rabobank.statementprocessor.dto;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@JacksonXmlRootElement(localName ="records")
public class Records implements Serializable {

    @JacksonXmlElementWrapper(localName = "record", useWrapping = false)
    private List<Record> record;

    public Records() {
    }
}
