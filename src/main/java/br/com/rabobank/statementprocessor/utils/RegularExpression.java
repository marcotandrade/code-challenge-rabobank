package br.com.rabobank.statementprocessor.utils;

public enum RegularExpression {

    VALIDATE_ONLY_NUMBER("[\\d]+"),
    VALIDATE_ALPHANUMERIC("[\\d\\w]+"),
    VALIDATE_MUTATION("[\\+\\d\\.]+|[\\-\\d\\.]+"),
    VALIDATE_BALANCE("[\\d\\.\\-]+");

    private String value;

    RegularExpression(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
