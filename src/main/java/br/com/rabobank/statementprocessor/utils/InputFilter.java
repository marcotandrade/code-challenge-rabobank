package br.com.rabobank.statementprocessor.utils;

public class InputFilter {

    public static boolean validateInput(String input, RegularExpression validationRegex){
        return input.matches(validationRegex.getValue());
    }

}
