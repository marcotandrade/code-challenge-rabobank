package br.com.rabobank.statementprocessor.enums;

import br.com.rabobank.statementprocessor.exception.FileNotSupportedException;
import br.com.rabobank.statementprocessor.exception.GenericException;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.NoSuchElementException;

@Getter
@AllArgsConstructor
public enum FileType {
    CSV("text/csv"),
    XML("application/xml");

    private String description;


    public static FileType lookup(String fileExtension) {
        try {
            String name = Arrays.stream(FileType.values()).filter(item -> item.getDescription().equalsIgnoreCase(fileExtension)).findFirst().get().name();
            return FileType.valueOf(name);
        } catch (NoSuchElementException e) {
            throw new FileNotSupportedException();
        } catch (Exception e) {
            throw new GenericException();
        }
    }

}
