package br.com.rabobank.statementprocessor.exception;

public class PropertyValidationException extends Exception {
    public PropertyValidationException() {
        super();
    }

    public PropertyValidationException(String message) {
        super(message);
    }
}
