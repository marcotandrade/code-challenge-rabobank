package br.com.rabobank.statementprocessor.exception.handler;

import br.com.rabobank.statementprocessor.dto.StandardError;
import br.com.rabobank.statementprocessor.exception.FileNotSupportedException;
import br.com.rabobank.statementprocessor.exception.GenericException;
import br.com.rabobank.statementprocessor.exception.PropertyValidationException;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
@RequiredArgsConstructor
public class ResourceExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(ResourceExceptionHandler.class);


    @ExceptionHandler(FileNotSupportedException.class)
    public ResponseEntity fileNotSupportedExceptionHandler(FileNotSupportedException ex, HttpServletRequest request) {
        LOGGER.error(String.format("fileNotSupportedExceptionHandler: %s" , ex.getMessage()));

        return ResponseEntity
                .status(HttpStatus.UNAUTHORIZED)
                .body(StandardError.builder()
                        .timestamp(System.currentTimeMillis())
                        .status(HttpStatus.UNAUTHORIZED.value())
                        .message("Ops! we do not work with this file type please try again with CSV or XML format")
                        .error(ex.getMessage())
                        .path(request.getRequestURI())
                        .build()
                );
    }


    @ExceptionHandler(GenericException.class)
    public ResponseEntity genericExceptionHandler(GenericException ex, HttpServletRequest request) {
        LOGGER.error(String.format("genericExceptionHandler: %s " , ex.getMessage()));

        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(StandardError.builder()
                                .timestamp(System.currentTimeMillis())
                                .status(HttpStatus.INTERNAL_SERVER_ERROR.value())
                                .message("Ops! we got a problem processing your file, please try again later.")
                                .error(ex.getMessage())
                                .path(request.getRequestURI())
                                .build()
                );
    }

    @ExceptionHandler(PropertyValidationException.class)
    public ResponseEntity propertyValidationExceptionHandler(PropertyValidationException ex, HttpServletRequest request) {
        LOGGER.error(String.format("propertyValidationExceptionHandler: %s" , ex.getMessage()));

        return ResponseEntity
                    .status(HttpStatus.UNAUTHORIZED)
                .body(StandardError.builder()
                                .timestamp(System.currentTimeMillis())
                                .status(HttpStatus.UNAUTHORIZED.value())
                                .message("The file has a record that does not complied with the specified format, please correct it and try again")
                                .error(ex.getMessage())
                                .path(request.getRequestURI())
                                .build()
                );
    }
}
