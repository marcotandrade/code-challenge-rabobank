package br.com.rabobank.statementprocessor.exception;

public class GenericException extends RuntimeException {
    public GenericException() {
        super();
    }

    public GenericException(String message) {
        super(message);
    }

    public GenericException(Throwable ex) {
        super(ex);
    }
}
