package br.com.rabobank.statementprocessor.exception;

public class FileNotSupportedException extends RuntimeException {
    public FileNotSupportedException() {
        super();
    }

    public FileNotSupportedException(String message) {
        super(message);
    }
}
