# Code Challenge Rabobank

## Project Info & Frameworks Used
- Java 11
- Spring Boot
- AWS S3
- H2 in memory database

## Deploy
start up the StatementprocessorApplication.java and the class will execute the project.

## Usage
The application will start on localhost:8080 you can use the below curl to configure
your request.

curl --request POST \
--url http://localhost:8080/file \
--header 'Content-Type: multipart/form-data; boundary=---011000010111000001101001' \
--form 'file=@/home/marcotulio/records.xml'

## Author
Marco Túlio Andrade Santos
